#pragma once
#include "CoreMinimal.h"
#include "AIController.h"
#include "Runtime/Engine/Classes/Engine/TargetPoint.h"
#include "Kismet/GameplayStatics.h"
#include "TagController.generated.h"


UCLASS()
class TAG_API ATagController : public AAIController {
	GENERATED_BODY()
public:
    void BeginPlay() override;
    void OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult& Result) override;
private:
    FTimerHandle TimerHandle;

    UPROPERTY()
        TArray<AActor*> Waypoints;

    UFUNCTION()
        ATargetPoint* GetRandomWaypoint();

    UFUNCTION()
        void GoToRandomWaypoint();
};
