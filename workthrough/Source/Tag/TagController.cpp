#include "TagController.h"

void ATagController::BeginPlay() {
    //GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("Start"));
    Super::BeginPlay();
    UGameplayStatics::GetAllActorsOfClass(GetWorld(), ATargetPoint::StaticClass(), Waypoints);
    GoToRandomWaypoint();
}

ATargetPoint* ATagController::GetRandomWaypoint() {
    auto index = FMath::RandRange(0, Waypoints.Num() - 1);
    return Cast<ATargetPoint>(Waypoints[index]);
}

void ATagController::GoToRandomWaypoint() {
    MoveToActor(GetRandomWaypoint());
}

void ATagController::OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult& Result) {
    Super::OnMoveCompleted(RequestID, Result);
    GetWorldTimerManager().SetTimer(TimerHandle, this, &ATagController::GoToRandomWaypoint, 1.0f, false);
}